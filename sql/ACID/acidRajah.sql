-- http://jira.vengeancewow.com/browse/TBC-1907
-- Rot Hide Gnolls cast Curse of Thule on themselves 
UPDATE `creature_ai_scripts` SET `action1_param2` = 1 WHERE `action1_type` = 11 AND `action1_param1` = 3237;

UPDATE `creature_template` SET `AIName` = 'EventAI' WHERE `entry` = 18046;
DELETE FROM `creature_ai_scripts` WHERE `creature_id` = 18046;
INSERT INTO `creature_ai_scripts` VALUES
(1804601, 18046, 0, 0, 100, 1, 0, 1000, 9000, 11000, 11, 35472, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'Rajah Haghazed - Cast Shield Charge'),
(1804602, 18046, 0, 0, 100, 1, 2000, 3000, 5000, 6000, 11, 16856, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Rajah Haghazed - Cast Mortal Strike'),
(1804603, 18046, 0, 0, 100, 1, 5000, 6000, 16000, 19000, 11, 35473, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Rajah Haghazed - Cast Forceful Cleave'),
(1804604, 18046, 2, 0, 100, 0, 25, 0, 0, 0, 11, 15062, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'Rajah Haghazed - Cast Shield Wall at 25% HP');