DELETE FROM `creature_ai_texts` WHERE `entry` BETWEEN -1458 AND -1442;
INSERT INTO `creature_ai_texts` VALUES
(-1442,'Activate defense mode EL-2S.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,'16504'),
(-1443,'Activate defense mode EL-5R.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,'16504'),
(-1444,'Activate defense mode EL-7M.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,'16504'),
(-1445,'How dare you spill the master''s wine!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,'16414 on Aggro'),
(-1446,'Impudent outsiders!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,'16414 on Aggro'),
(-1447,'Meddling fools! You will pay with your lives!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,'16414 on Aggro'),
(-1448,'You''ll never make it out alive...',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,'16414 on Death'),
(-1449,'I mustn''t be afraid...',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,'16410 on Death'),
(-1450,'Off with you!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,'16410 on Aggro'),
(-1451,'You''ll ruin everything!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,'16410 on Aggro'),
(-1452,'The rest are fine without us.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,'16407 idle'),
(-1453,'They''ll never know we''re missing.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,'16407 idle'),
(-1454,'If we dawdle much longer, there''ll be hell to pay!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,'16407 idle'),
(-1455,'What is this?',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,'16407 on Aggro'),
(-1456,'Invaders in the tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,'16407 on Aggro'),
(-1457,'Intruders!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,'15551 on Aggro'),
(-1458,'You are not welcome!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,'15551 on Aggro');

/*
Spectral Charger (15547)
- Now casts Fear after casting Charge
Charge animation looks weird to me (too fast maybe)
*/
DELETE FROM `creature_ai_scripts` WHERE `creature_id` = 15547;
INSERT INTO `creature_ai_scripts` VALUES
(1554701,15547,0,0,100,3,5000,9000,13000,17000,11,29320,5,0,22,1,0,0,0,0,0,0,'Spectral Charger - Cast Charge and Set Phase 1'),
(1554702,15547,0,5,100,3,0,0,0,0,11,29321,4,0,22,0,0,0,0,0,0,0,'Spectral Charger - Cast Fear and Set Phase 0 (Phase 1)');

/* Phantom Guest (add females) */
UPDATE `creature_model_info` SET `modelid_other_gender`= 16466 WHERE `modelid` = 16464;
UPDATE `creature_model_info` SET `modelid_other_gender`= 16467 WHERE `modelid` = 16465;

/* Retainers, Guests, Attendants dance emote */ 
DELETE FROM `creature_addon` WHERE `guid` IN (85183,85184,85185,85186,85187,85188,85190,85193,85194,85195,85199,85203,85204,85206,85208,85209,85211,85133,85134,85126,85127,85128,85129,85130,85131,85132,85119,85118,85175,85176,85121,85122,85120,85191,85189,85102,85101,85100,85108,85107,85106,85105,85104,85103,85109,85110,85111,85112,85113,85114,85115,85116,85117,85095,85096,85097,85098,85099,85089,85090,85091,85092,85093,85123,85124,85177);
INSERT INTO `creature_addon` VALUES
(85183,0,0,0,0,400,0,''),
(85184,0,0,0,0,400,0,''),
(85185,0,0,0,0,400,0,''),
(85186,0,0,0,0,400,0,''),
(85187,0,0,0,0,400,0,''),
(85188,0,0,0,0,400,0,''),
(85190,0,0,0,0,400,0,''),
(85193,0,0,0,0,400,0,''),
(85194,0,0,0,0,400,0,''),
(85195,0,0,0,0,400,0,''),
(85199,0,0,0,0,400,0,''),
(85203,0,0,0,0,400,0,''),
(85204,0,0,0,0,400,0,''),
(85206,0,0,0,0,400,0,''),
(85208,0,0,0,0,400,0,''),
(85209,0,0,0,0,400,0,''),
(85211,0,0,0,0,400,0,''),
(85133,0,0,0,0,400,0,''),
(85134,0,0,0,0,400,0,''),
(85126,0,0,0,0,400,0,''),
(85127,0,0,0,0,400,0,''),
(85128,0,0,0,0,400,0,''),
(85129,0,0,0,0,400,0,''),
(85130,0,0,0,0,400,0,''),
(85131,0,0,0,0,400,0,''),
(85132,0,0,0,0,400,0,''),
(85119,0,0,0,0,400,0,''),
(85118,0,0,0,0,400,0,''),
(85175,0,0,0,0,400,0,''),
(85176,0,0,0,0,400,0,''),
(85121,0,0,0,0,400,0,''),
(85122,0,0,0,0,400,0,''),
(85120,0,0,0,0,400,0,''),
(85191,0,0,0,0,400,0,''),
(85189,0,0,0,0,400,0,''),
(85102,0,0,0,0,400,0,''),
(85101,0,0,0,0,400,0,''),
(85100,0,0,0,0,400,0,''),
(85108,0,0,0,0,400,0,''),
(85107,0,0,0,0,400,0,''),
(85106,0,0,0,0,400,0,''),
(85105,0,0,0,0,400,0,''),
(85104,0,0,0,0,400,0,''),
(85103,0,0,0,0,400,0,''),
(85109,0,0,0,0,400,0,''),
(85110,0,0,0,0,400,0,''),
(85111,0,0,0,0,400,0,''),
(85112,0,0,0,0,400,0,''),
(85113,0,0,0,0,400,0,''),
(85114,0,0,0,0,400,0,''),
(85115,0,0,0,0,400,0,''),
(85116,0,0,0,0,400,0,''),
(85117,0,0,0,0,400,0,''),
(85095,0,0,0,0,400,0,''),
(85096,0,0,0,0,400,0,''),
(85097,0,0,0,0,400,0,''),
(85098,0,0,0,0,400,0,''),
(85099,0,0,0,0,400,0,''),
(85089,0,0,0,0,400,0,''),
(85090,0,0,0,0,400,0,''),
(85091,0,0,0,0,400,0,''),
(85092,0,0,0,0,400,0,''),
(85093,0,0,0,0,400,0,''),
(85123,0,0,0,0,400,0,''),
(85124,0,0,0,0,400,0,''),
(85177,0,0,0,0,400,0,'');

/* Spectral Retainer (more say text) */
DELETE FROM `creature_ai_scripts` WHERE `creature_id` = 16410;
INSERT INTO `creature_ai_scripts` VALUES
('1641001','16410','9','0','100','3','0','5','6000','12000','11','29578','4','32','0','0','0','0','0','0','0','0','Spectral Retainer - Cast Rend'),
('1641002','16410','0','0','100','3','9000','13000','18000','22000','11','29546','5','1','0','0','0','0','0','0','0','0','Spectral Retainer - Cast Oath of Fealty'),
('1641003','16410','4','0','50','2','0','0','0','0','1','-1450','-1451','0','0','0','0','0','0','0','0','0','Spectral Retainer - Random Say on Aggro'),
('1641004','16410','6','0','20','2','0','0','0','0','1','-1449','0','0','0','0','0','0','0','0','0','0','Spectral Retainer - Say on Death');

/* Spectral Servant (more say text) */
DELETE FROM `creature_ai_scripts` WHERE `creature_id` = 16407;
INSERT INTO `creature_ai_scripts` VALUES
('1640701', '16407', '9', '0', '100', '2', '0', '40', '12000', '21000', '11', '29540', '1', '32', '0', '0', '0', '0', '0', '0', '0', '0', 'Spectral Servant - Cast Curse of Past Burdens'),
('1640702', '16407', '6', '0', '20', '2', '0', '0', '0', '0', '1', '-43', '-44', '-45', '0', '0', '0', '0', '0', '0', '0', '0', 'Spectral Servant - Random Say on Death'),
('1640703', '16407','1','0','10','2','300000','600000','300000','600000','1','-1452','-1453','-1454','0','0','0','0','0','0','0','0','Spectral Servant - Random Say Idle OOC'),
('1640704', '16407','4','0','10','2','0','0','0','0','1','-1455','-1456','0','0','0','0','0','0','0','0','0','Spectral Servant - Random Say on Aggro');

/* Spectral Stable Hand (more say text) */
DELETE FROM `creature_ai_scripts` WHERE `creature_id` = 15551;
INSERT INTO `creature_ai_scripts` VALUES
('1555101', '15551', '9', '0', '100', '3', '0', '5', '11000', '15000', '11', '18812', '4', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Spectral Stable Hand - Cast Knockdown'),
('1555102', '15551', '0', '0', '100', '3', '4000', '8000', '45000', '48000', '11', '6016', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', 'Spectral Stable Hand - Cast Pierce Armor'),
('1555103', '15551', '14', '0', '100', '2', '38400', '30', '21000', '28000', '11', '29339', '6', '1', '11', '29340', '6', '0', '0', '0', '0', '0', 'Spectral Stable Hand - Cast Healing Touch and Whip Rage on Friendlies'),
('1555104', '15551', '6', '0', '100', '2', '0', '0', '0', '0', '1', '-40', '-41', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Spectral Stable Hand - Random Say on Death'),
('1555105','15551','4','0','25','2','0','0','0','0','1','-1457','-1458','0','0','0','0','0','0','0','0','0','Spectral Stable Hand - Random Say on Aggro');

/*
Mana Feeder (16491)
http://www.wowhead.com/npc=16491/mana-feeder
- Added immunity to all magic
*/

UPDATE `creature_template` SET `SchoolImmuneMask` = 126 WHERE `Entry` = 16491;

/* 
Arcane Protector (16504)
http://www.wowhead.com/npc=16504/arcane-protector
http://wowwiki.wikia.com/wiki/Arcane_Protector
http://www.wowhead.com/spell=29815/return-fire (This is the actual missle that does damage - triggered by 29788, 29793, 29794)
http://www.wowhead.com/spell=29788/return-fire (Counterattack when hit by melee)
http://www.wowhead.com/spell=29793/return-fire (Counterattack when hit by spells)
http://www.wowhead.com/spell=29794/return-fire (Counterattack when hit by ranged)
- Added immunity to stuns and snares
- Now casts Fist of Stone and Astral Spark in all phases, and switches between different defense modes
It's possible there should be more spawns of Arcane Protector instead of Arcane Watchman
http://www.wowhead.com/npc=16485/arcane-watchman#comments:id=44888 (Implies there are only 2 pairs of Arcane Watchman)
select * from creature where id = 16504; -- (Only 1 Arcane Protector currently)
select * from creature where id = 16485; -- (7 Arcane Watchman)
guids 85271, 85277, 85312 These 3 should be Protectors probably 
http://www.wowhead.com/npc=16485/arcane-watchman#comments:id=44888:reply=36158 (Pair at Guardian's Library entrance may be skipped by hugging wall)
*/

UPDATE `creature_template` SET `MechanicImmuneMask` = 19456 WHERE `Entry` = 16504;

DELETE FROM `creature_ai_scripts` WHERE `creature_id` = 16504;
INSERT INTO `creature_ai_scripts` (`id`,`creature_id`,`event_type`,`event_inverse_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action1_type`,`action1_param1`,`action1_param2`,`action1_param3`,`action2_type`,`action2_param1`,`action2_param2`,`action2_param3`,`action3_type`,`action3_param1`,`action3_param2`,`action3_param3`,`comment`) VALUES
(1650401, 16504, 11, 0, 100, 2, 0, 0, 0, 0, 11, 11649, 0, 1, 30, 1, 2, 3, 0, 0, 0, 0, 'Arcane Protector - Cast Detect Invisibility on Spawn'),
(1650402, 16504, 0, 13, 100, 3, 0, 0, 0, 0, 11, 29788, 0, 1, 1, -1444, 0, 0, 22, 0, 0, 0, 'Arcane Protector - Cast Return Fire (Phase 1)'),
(1650403, 16504, 0, 11, 100, 3, 0, 0, 0, 0, 11, 29793, 0, 1, 1, -1442, 0, 0, 22, 0, 0, 0, 'Arcane Protector - Cast Return Fire (Phase 2)'),
(1650404, 16504, 0, 7, 100, 3, 0, 0, 0, 0, 11, 29794, 0, 1, 1, -1443, 0, 0, 22, 0, 0, 0, 'Arcane Protector - Cast Return Fire (Phase 3)'),
(1650405, 16504, 0, 0, 100, 3, 3000, 8000, 15000, 19000, 12, 17283, 4, 600000, 0, 0, 0, 0, 0, 0, 0, 0, 'Arcane Protector - Summon Astral Spark'),
(1650406, 16504, 0, 0, 100, 3, 1000, 3000, 18000, 21000, 11, 29840, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Arcane Protector - Cast Fist of Stone'),
(1650407, 16504, 0, 0, 100, 3, 0, 0, 21000, 22000, 30, 1, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 'Arcane Protector - Set Random Phase');

-- These 3 patrols should be Protectors instead of Watchman
UPDATE `creature` SET `id` = 16504 WHERE `guid` IN (85271, 85277, 85312);

/*
Ghostly Steward (16414)
http://www.wowhead.com/npc=16414/ghostly-steward#comments:id=41030
http://www.wowhead.com/npc=16414/ghostly-steward#comments:id=32842:reply=2230
http://wowwiki.wikia.com/wiki/Ghostly_Steward
- Now casts Frenzy at 50% (up from 25%)
- Added immunity to taunt, stun, shackle, and freeze trap
- Made Drunken Skull Crack happen a little sooner first time after Frenzy? (just guessing)
*/

UPDATE `creature_template` SET `ExtraFlags` = 256, `MechanicImmuneMask` = 8919056 WHERE `Entry` = 16414;

DELETE FROM `creature_ai_scripts` WHERE `creature_id` = 16414;
INSERT INTO `creature_ai_scripts` (`id`,`creature_id`,`event_type`,`event_inverse_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action1_type`,`action1_param1`,`action1_param2`,`action1_param3`,`action2_type`,`action2_param1`,`action2_param2`,`action2_param3`,`action3_type`,`action3_param1`,`action3_param2`,`action3_param3`,`comment`) VALUES
(1641401, 16414, 2, 0, 100, 2, 50, 0, 0, 0, 11, 29691, 0, 0, 1, -350, -351, -352, 23, 1, 0, 0, 'Ghostly Steward - Cast Frenzy and Yell at 50% HP'),
(1641402, 16414, 0, 1, 100, 3, 1000, 4000, 7000, 14000, 11, 29690, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Ghostly Steward - Cast Drunken Skull Crack (Phase 1)'),
(1641403, 16414, 6, 0, 20, 2, 0, 0, 0, 0, 1, -49, -1448, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Ghostly Steward - Say on Death'),
(1641404, 16414, 4, 0, 20, 2, 0, 0, 0, 0, 1, -1445, -1446, -1447, 0, 0, 0, 0, 0, 0, 0, 0, 'Ghostly Steward - Say on Aggro');