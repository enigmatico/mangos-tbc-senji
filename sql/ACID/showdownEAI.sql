INSERT INTO `creature_ai_texts` (`entry`, `content_default`, `sound`, `type`, `language`, `emote`, `comment`) VALUES
(-1433, 'Your father destroyed my children and left their bodies impaled upon the rocky blades at Dragon''s End. For this, you will die! Enough of this farce. Prepare to face my full wrath!', 0, 1, 0, 0, 'Baron Sablemane'),
(-1434, 'Baron Sablemane begins emanating immense power.', 0, 2, 0, 0, 'Baron Sablemane');
 
INSERT INTO `creature_ai_scripts` (`id`,`creature_id`,`event_type`,`event_inverse_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action1_type`,`action1_param1`,`action1_param2`,`action1_param3`,`action2_type`,`action2_param1`,`action2_param2`,`action2_param3`,`action3_type`,`action3_param1`,`action3_param2`,`action3_param3`,`comment`) VALUES
(2055501,20555, 0, 0, 100, 1, 6000, 11000, 10000, 15000, 11, 38783, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Goc - Cast Boulder Volley'),
(2055502,20555, 0, 0, 100, 1, 8000, 10000, 15000, 20000, 11, 38784, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Goc - Cast Ground Smash'),
(2055503,20555, 9, 0, 100, 1, 0, 8, 1000, 1000, 49, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Goc - Disable Dynamic Movement at 0-8 Yards'),
(2055504,20555, 9, 0, 100, 1, 9, 80, 1000, 1000, 49, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Goc - Enable Dynamic Movement at 9-80 Yards'),
(2055505,20555, 4, 0, 100, 0, 0, 0, 0, 0, 19, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Goc - Remove Non-Attackable Flag on Aggro'),

(2247301,22473, 7, 0, 100, 0, 0, 0, 0, 0, 22, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Baron Sablemane - Set Phase to 0 on Evade'),
(2247302,22473, 3, 11, 100, 1, 100, 15, 1000, 1000, 22, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Baron Sablemane - Set Phase 1 when Mana is above 15% (Phase 2)'),
(2247303,22473, 3, 13, 100, 0, 7, 0, 0, 0, 49, 0, 0, 0, 22, 2, 0, 0, 0, 0, 0, 0, 'Baron Sablemane - Disable Dynamic Movement and Set Phase 2 when Mana is at 7% (Phase 1)'),
(2247304,22473, 9, 0, 100, 1, 0, 8, 1000, 1000, 49, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Baron Sablemane - Disable Dynamic Movement at 0-8 Yards'),
(2247305,22473, 9, 13, 100, 1, 9, 80, 1000, 1000, 49, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Baron Sablemane - Enable Dynamic Movement at 9-80 Yards (Phase 1)'),
(2247306,22473, 9, 13, 100, 1, 0, 40, 3300, 5000, 11, 17290, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Baron Sablemane - Cast Fireball (Phase 1)'),
(2247307,22473, 4, 0, 100, 0, 0, 0, 0, 0, 49, 1, 0, 0, 22, 1, 0, 0, 0, 0, 0, 0, 'Baron Sablemane - Enable Dynamic Movement and Set Phase 1 on Aggro'),
(2247308,22473, 9, 13, 100, 1, 0, 40, 15400, 23500, 11, 39268, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'Baron Sablemane - Cast Chains of Ice (Phase 1)'),
(2247309,22473, 12, 0, 100, 0, 15, 0, 0, 0, 22, 3, 0, 0, 1, -1433, 0, 0, 49, 0, 0, 0, 'Baron Sablemane - Set Phase 3, Disable Dynamic Movement, and speak at 15% HP'),
(2247310,22473, 12, 0, 100, 0, 15, 0, 0, 0, 11, 39255, 0, 1, 1, -1434, 0, 0, 36, 22496, 0, 0, 'Baron Sablemane - Play emote, Cast Black Dragon Form and Transform at 15% HP'),
(2247311,22473, 0, 7, 100, 1, 3000, 4000, 6500, 7500, 11, 39263, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Baron Sablemane - Cast Flame Breath (Phase 3)'),

(2244801,22448, 0, 0, 100, 1, 1000, 2000, 7000, 8000, 11, 40504, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Rexxar - Cast Cleave'),
(2244802,22448, 0, 0, 100, 1, 3000, 3000, 5000, 6000, 11, 3391, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Rexxar - Cast Thrash'),
(2244803,22448, 2, 0, 100, 0, 40, 0, 0, 0, 12, 22498, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Rexxar - Summon Misha at 40% HP'),
(2244804,22448, 25, 0, 100, 0, 22498, 0, 0, 0, 11, 8602, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'Rexxar - Cast Vengeance on Misha Death'),

(2249801,22498, 0, 0, 100, 1, 1000, 2000, 5000, 6000, 11, 17156, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Misha - Cast Maul'),
(2249802,22498, 0, 0, 100, 1, 3000, 4000, 25000, 30000, 11, 20753, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Misha - Cast Demoralizing Roar');


-- UPDATE 23.08.
-- http://jira.vengeancewow.com/browse/TBC-1799 
UPDATE `creature_template` SET `AIName` = 'EventAI', `Armor` = 6116, `MinMeleeDmg` = 214, `MaxMeleeDmg` = 301, `MeleeAttackPower` = 276, `DamageMultiplier` = 1, `MinRangedDmg` = 179, `MaxRangedDmg` = 266 WHERE `entry` = 22025;
DELETE FROM `creature_ai_scripts` WHERE `creature_id` = 22025;
INSERT INTO `creature_ai_scripts` (`id`,`creature_id`,`event_type`,`event_inverse_phase_mask`,`event_chance`,`event_flags`,
`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action1_type`,`action1_param1`,`action1_param2`,`action1_param3`,`action2_type`,`action2_param1`,`action2_param2`,`action2_param3`,`action3_type`,`action3_param1`,`action3_param2`,`action3_param3`,`comment`) VALUES 
(2202501,22025,0,0,100,1,1000,2000,5000,5000,11,15496,0,0,0,0,0,0,0,0,0,0,'Asghar - Cast Cleave'),
(2202502,22025,2,0,100,1,50,0,6000,7000,11,16588,0,0,0,0,0,0,0,0,0,0,'Asghar - Cast Dark Mending at 50% HP');

-- http://jira.vengeancewow.com/browse/TBC-1770 (Demon Hunter Supplicant)
UPDATE `creature_template` SET `AIName` = 'EventAI' WHERE `entry` = 21179;
DELETE FROM `creature_ai_scripts` WHERE `creature_id` = 21179;
INSERT INTO `creature_ai_scripts` (`id`,`creature_id`,`event_type`,`event_inverse_phase_mask`,`event_chance`,`event_flags`,
`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action1_type`,`action1_param1`,`action1_param2`,`action1_param3`,`action2_type`,`action2_param1`,`action2_param2`,`action2_param3`,`action3_type`,`action3_param1`,`action3_param2`,`action3_param3`,`comment`) VALUES 
(2117901,21179,2,0,100,0,40,0,0,0,1,-1459,0,0,11,37683,0,0,0,0,0,0,'Demon Hunter Supplicant - Cast Evasion at 40% HP'),
(2117902,21179,2,0,100,0,25,0,0,0,11,32720,0,0,0,0,0,0,0,0,0,0,'Demon Hunter Supplicant - Cast Sprint at 25% HP');
INSERT INTO `creature_ai_texts` (`entry`, `content_default`, `sound`, `type`, `language`, `emote`, `comment`) VALUES
(-1459,'Demon Hunter Supplicant begins intensely dodging incoming attacks.',0,2,0,0,'Demon Hunter Supplicant');

-- http://jira.vengeancewow.com/browse/TBC-1002
-- http://jira.vengeancewow.com/browse/TBC-1313
UPDATE `creature_ai_scripts` SET `action1_param2` = 4 WHERE `creature_id` = 18796 AND `id` = 1879605;

-- UPDATE 26 08
INSERT INTO creature_ai_scripts VALUES
('2110103', '21101', '11', '0', '100', '0', '0', '0', '0', '0', '21', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Unbound Void Zone (Normal) - Do not move in combat');
DELETE FROM creature_ai_scripts WHERE creature_id=16697;
INSERT INTO creature_ai_scripts VALUES
('1669701', '16697', '11', '0', '100', '0', '0', '0', '0', '0', '21', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Void Zone (Netherspite) - Do not move in combat'),
('1669702', '16697', '11', '0', '100', '0', '0', '0', '0', '0', '11', '32250', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Void Zone (Netherspite) - Cast Consumption on Spawn');

-- update 1 09

INSERT INTO `creature_ai_texts` (`entry`,`content_default`,`sound`,`type`,`language`,`emote`,`comment`) VALUES
(-1460,'Who dares to interrupt my operations?',0,1,0,0,'Nexus-Prince Razaan spawn');

DELETE FROM `creature_ai_scripts` WHERE `creature_id` IN (21057,20601,20609,20614);
INSERT INTO `creature_ai_scripts` (`id`,`creature_id`,`event_type`,`event_inverse_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action1_type`,`action1_param1`,`action1_param2`,`action1_param3`,`action2_type`,`action2_param1`,`action2_param2`,`action2_param3`,`action3_type`,`action3_param1`,`action3_param2`,`action3_param3`,`comment`) VALUES
(2105701,21057,1,0,100,0,1000,1000,0,0,1,-1460,0,0,0,0,0,0,0,0,0,0,'Nexus-Prince Razaan yell on spawn'),
(2105702,21057,0,0,100,1,8000,12000,17000,21000,11,35924,0,0,0,0,0,0,0,0,0,0,'Nexus-Prince Razaan - Cast Energy Flux'),
(2060101,20601,0,0,100,1,7000,12000,9000,15000,11,35922,0,1,0,0,0,0,0,0,0,0,'Razaani Raider - Cast Energy Flare'),
(2060102,20601,0,0,100,1,2000,6000,16000,21000,11,32920,1,1,0,0,0,0,0,0,0,0,'Razaani Raider - Cast Warp'),
(2060901,20609,9,0,100,1,0,8,12000,15000,11,11975,0,0,0,0,0,0,0,0,0,0,'Razaani Nexus Stalker - Cast Arcane Explosion'),
(2060902,20609,9,0,100,1,0,30,14000,18000,11,36513,0,0,0,0,0,0,0,0,0,0,'Razaani Nexus Stalker - Cast Intangible Presence'),
(2061401,20614,9,0,100,1,0,8,9000,14000,11,36508,1,0,0,0,0,0,0,0,0,0,'Razaani Spell-Thief - Cast Energy Surge');

-- 04 09

DELETE FROM `creature_ai_scripts` WHERE `creature_id` IN (18482,18483);
INSERT INTO `creature_ai_scripts` (`id`,`creature_id`,`event_type`,`event_inverse_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action1_type`,`action1_param1`,`action1_param2`,`action1_param3`,`action2_type`,`action2_param1`,`action2_param2`,`action2_param3`,`action3_type`,`action3_param1`,`action3_param2`,`action3_param3`,`comment`) VALUES 
(1848201,18482,11,0,100,0,0,0,0,0,42,1,1,0,0,0,0,0,0,0,0,0,'Empoor - Set Invincibility at 1% on Spawn'),
(1848202,18482,4,0,100,0,0,0,0,0,11,12550,0,0,0,0,0,0,0,0,0,0,'Empoor - Cast Lightning Shield on aggro'),
(1848203,18482,0,0,100,1,1000,2000,8000,9000,11,12548,1,0,0,0,0,0,0,0,0,0,'Empoor - Cast Frost Shock'),
(1848204,18482,2,0,100,0,25,0,0,0,24,0,0,0,22,1,0,0,0,0,0,0,'Empoor - Set Phase 1, flee, and evade at 25% HP'),
(1848205,18482,21,5,100,0,0,0,0,0,2,35,0,0,22,2,0,0,17,168,2,0,'Empoor - Become Friendly and Set Phase 2 on Reach Home and add Quest giver flag (Phase 1)'),
(1848206,18482,1,3,100,1,40000,40000,40000,40000,22,0,0,0,41,0,0,0,17,168,1,0,'Empoor - Despawn after 40 seconds OOC and Set Phase 0 and add Gossip flag (Phase 2)'),

(18483001,18483,0,0,100,1,2000,2000,15000,16000,11,11977,1,0,0,0,0,0,0,0,0,0,'Empoor''s Bodyguard - Cast Rend'),
(18483002,18483,0,0,100,1,1000,1000,20000,21000,11,13730,1,0,0,0,0,0,0,0,0,0,'Empoor''s Bodyguard - Cast Demoralizing Shout');