DELETE FROM `creature_ai_scripts` WHERE `creature_id` IN (14502, 14506);
INSERT INTO `creature_ai_scripts` (`id`,`creature_id`,`event_type`,`event_inverse_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action1_type`,`action1_param1`,`action1_param2`,`action1_param3`,`action2_type`,`action2_param1`,`action2_param2`,`action2_param3`,`action3_type`,`action3_param1`,`action3_param2`,`action3_param3`,`comment`) VALUES
(1450201, 14502, 0, 0, 100, 2, 1000, 1000, 0, 0, 11, 16636, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Xorothian Dreadsteed - Cast Berserker Charge on Aggro'),
(1450202, 14502, 0, 0, 100, 3, 4000, 8000, 20000, 25000, 11, 22713, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Xorothian Dreadsteed - Cast Flame Buffet'),
(1450203, 14502, 2, 0, 100, 2, 50, 0, 0, 0, 12, 14506, 1, 30000, 0, 0, 0, 0, 0, 0, 0, 0, 'Xorothian Dreadsteed - Summon Lord Hel''nurath at 50% HP'),
(1450204, 14502, 6, 0, 100, 2, 0, 0, 0, 0, 11, 23159, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 'Xorothian Dreadsteed - Summon Dreadsteed Spirit on Death'),

(1450601, 14506, 0, 0, 100, 3, 5000, 9000, 22000, 25000, 11, 17146, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Lord Hel''nurath - Cast Shadow Word: Pain'),
(1450602, 14506, 0, 0, 100, 3, 11000, 14000, 14000, 20000, 11, 23224, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'Lord Hel''nurath - Cast Veil of Shadow'),
(1450603, 14506, 0, 0, 100, 3, 4000, 9000, 9000, 15000, 11, 18670, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Lord Hel''nurath - Cast Knock Away'),
(1450604, 14506, 0, 0, 100, 3, 9000, 13000, 14000, 17000, 11, 20989, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'Lord Hel''nurath - Cast Sleep'),
(1450605, 14506, 4, 0, 100, 2, 0, 0, 0, 0, 1, -1461, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Lord Hel''nurath - Yell on Aggro');

DELETE FROM `creature_ai_texts` WHERE `entry` = -1461;
INSERT INTO `creature_ai_texts` (`entry`,`content_default`,`content_loc1`,`content_loc2`,`content_loc3`,`content_loc4`,`content_loc5`,`content_loc6`,`content_loc7`,`content_loc8`,`sound`,`type`,`language`,`emote`,`comment`) VALUES
(-1461,'Who dares steal my precious mount? You will pay for your insolence, mortal!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1,0,0,'Lord Hel''nurath - Yell on Aggro');